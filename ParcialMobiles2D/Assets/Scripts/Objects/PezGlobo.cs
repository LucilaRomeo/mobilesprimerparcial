using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PezGlobo : MonoBehaviour
{
    public float tiempoVelocidadReducida = 3f;
    private float lifeTime = 3f;

    private void OnEnable()
    {

        Invoke("Deactivate", lifeTime);
    }

    private void OnDisable()
    {
        CancelInvoke();
    }

    private void Deactivate()
    {
        gameObject.SetActive(false);
    }

 

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Gyroscope movimientoJugador = collision.gameObject.GetComponent<Gyroscope>();
            if (movimientoJugador != null)
            {
                movimientoJugador.aplicarEfecto("PezGlobo");
                Vibrar();
                Deactivate();
            }
        }
    }
  
    private void Vibrar()
    {
#if UNITY_ANDROID || UNITY_IOS
        Handheld.Vibrate();
#endif
    }
}

