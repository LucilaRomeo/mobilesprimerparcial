using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perlas : MonoBehaviour
{
    public int aumentoVida;
    private float lifeTime = 3f;

    private void OnEnable()
    {

        Invoke("Deactivate", lifeTime);
    }

    private void OnDisable()
    {
        CancelInvoke();
    }

    private void Deactivate()
    {
        gameObject.SetActive(false);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        Damage damage = collision.GetComponent<Damage>();
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.GetComponent<Damage>().AumentarVida(aumentoVida);
            Deactivate();

        }
    }

}
