using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocas : MonoBehaviour
{
    private float lifeTime = 3f;
    private void OnEnable()
    {

        Invoke("Deactivate", lifeTime);
    }

    private void OnDisable()
    {
        CancelInvoke();
    }

    private void Deactivate()
    {
        gameObject.SetActive(false);
    }

 
    public void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("Player"))
        {
            collision.GetComponent<Damage>().RecibirDamage(100);
            Vibrar();
            Debug.Log("Perdiste");
            Deactivate();

        }
    }

    private void Vibrar()
    {
#if UNITY_ANDROID || UNITY_IOS
        Handheld.Vibrate();
#endif
    }
}
