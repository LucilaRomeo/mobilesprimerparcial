using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gemas : MonoBehaviour
{
    public float duracionInvulnerabilidad = 5f;
    private float lifeTime = 3f;
    private void OnEnable()
    {
        Invoke("Deactivate", lifeTime);
    }

    private void OnDisable()
    {
        CancelInvoke();
    }

    private void Deactivate()
    {
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
       
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.GetComponent<Damage>().DuracionActual = duracionInvulnerabilidad;
            Deactivate();
        }
    }


}
