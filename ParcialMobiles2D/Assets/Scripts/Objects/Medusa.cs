using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Medusa : MonoBehaviour
{
    public int da�o;
    private float lifeTime = 3f;

    private void OnEnable()
    {

        Invoke("Deactivate", lifeTime);
    }

    private void OnDisable()
    {
        CancelInvoke();
    }

    private void Deactivate()
    {
        gameObject.SetActive(false);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Damage damage = collision.GetComponent<Damage>();
            if (damage != null)
            {
                damage.RecibirDamage(da�o);
                Vibrar();
                gameObject.SetActive(false);
                Destroy(gameObject);
                Destroy(gameObject);
            }
        }
    }

    private void Vibrar()
    {
#if UNITY_ANDROID || UNITY_IOS
        Handheld.Vibrate();
#endif
    }
}
