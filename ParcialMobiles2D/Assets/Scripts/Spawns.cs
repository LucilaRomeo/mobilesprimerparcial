using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawns : MonoBehaviour
{
    public float Velocity;
    private bool movimientoIzq;
    private float Minimo = -9f;
    private float Maximo = 9f;
    public Transform spawnPoint;
    private float intervaloMin = 1f;
    private float intervaloMax = 3f;
    private ObjectPooler objectPooler;

    private string[] objectTags = { "Medusa", "Gema", "Moneda", "PezGlobo", "Roca", "Perla" };

    public void Start()
    {
        objectPooler = ObjectPooler.instance;
        StartCoroutine(SpawnRandomObjectCoroutine());
    }

    private void Update()
    {
        if (this.transform.position.x >= Maximo)
        {
            movimientoIzq = true;
        }
        if (this.transform.position.x <= Minimo)
        {
            movimientoIzq = false;
        }
        if (movimientoIzq)
        {
            MoverIzquierda();
        }
        else
        {
            MoverDerecha();
        }
    }

    public void MoverIzquierda()
    {
        this.transform.position += -transform.right * Velocity * Time.deltaTime;
    }

    public void MoverDerecha()
    {
        this.transform.position += transform.right * Velocity * Time.deltaTime;
    }

    private IEnumerator SpawnRandomObjectCoroutine()
    {
        while (true)
        {
            // Espera un intervalo aleatorio entre spawns
            yield return new WaitForSeconds(Random.Range(intervaloMin, intervaloMax));

            // Generar una posici�n aleatoria dentro del �rea de spawn
            Vector3 spawnPosition = spawnPoint.position;
            spawnPosition.x += Random.Range(-3f, 3f);

            // Elegir un objeto aleatorio del array de tags
            string randomTag = objectTags[Random.Range(0, objectTags.Length)];

            // Usar el ObjectPooler para spawnear el objeto
            objectPooler.SpawnFromPool(randomTag, spawnPosition, Quaternion.identity);
        }
    }

}
