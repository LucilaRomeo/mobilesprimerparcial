using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gyroscope : MonoBehaviour
{
    Rigidbody2D rb;
    float DirY;
    public float MoveSpeed = 20f;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        DirY = Input.acceleration.x * MoveSpeed;
        Vector2 clampedPosition = transform.position;
        clampedPosition.x = Mathf.Clamp(transform.position.x, -9.5f, 9.5f);
        transform.position = clampedPosition;
    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(DirY, 0f);
    }
    public void aplicarEfecto(string efecto)
    {
        switch (efecto)
        {
            case "PezGlobo":
                MoveSpeed = 0f;
                StartCoroutine(ReducirVelocidadTemporalmente());
                break;
            default: break;
        }
      
    }
    IEnumerator ReducirVelocidadTemporalmente()
    {
        yield return new WaitForSeconds(3f);
       MoveSpeed = 20f;

    }
}
