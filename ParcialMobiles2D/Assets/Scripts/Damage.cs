using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Damage : MonoBehaviour
{
    //public Slider BarraVida;
    public int cantidadVida = 100;
    public GameObject perdisteCanvas;
    public float DuracionInvulnerabilidad;
    public float DuracionActual;
    private void Start()
    {
        perdisteCanvas.SetActive(false);
    }
    private void Update()
    {
        if(DuracionActual > 0)
            DuracionActual -= Time.deltaTime;
    }
    public void RecibirDamage(int damage)
    {
        if (DuracionActual > 0)
            return;

        cantidadVida -= damage;

        if (cantidadVida <= 0)
        {
            Morir();
            Debug.Log("Perdiste2");
        }
    }
    public void AumentarVida(int masVida)
    {
        cantidadVida += masVida;
        if (cantidadVida >= 100)
            cantidadVida = 100;
    }

    public void Morir()
    {
        perdisteCanvas.SetActive(true);
    }
  

}
