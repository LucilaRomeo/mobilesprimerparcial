using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    public TMPro.TextMeshProUGUI PlataTXT;
    public int Dinero;
    void Awake()
    {
        if (Instance != null)
        {
            Destroy(Instance);
            Instance = this;
        }
        else
        {
            Instance = this;

        }

    }
    public void sumarDinero(int suma)
    {
        Dinero += suma;
        Debug.Log(Dinero);
        PlataTXT.text = Dinero.ToString();

    }

}
